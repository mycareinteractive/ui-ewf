ewf.dialogView = {
    handler: null,
    panelHeight: 300,
    dialogpanel_div: document.createElement("dialogpanel_div"),
    CheckoutStatus: false,
    btn: 1,
    setCheckoutStatus: function (CheckoutStatus) {
        ewf.dialogView.CheckoutStatus = CheckoutStatus;
    },

    getCheckoutStatus: function () {
        return ewf.dialogView.CheckoutStatus;
    },

    DialogPanelBegin: function (content, mode, time, callback) {
        ewf.dialogView.hasEnterCapsLock = false;
        ewf.dialogView.hasEnterTab = false;
        ewf.SpecialEventHandler.SpecialEventFlag = false;
        var obj = document.createElement("dialogpanel_background");
        obj.setAttribute('id', 'dialogpanel_background');
        document.body.appendChild(obj);
        this.callback = callback;
        this.mode = mode;
        ewf.dialogView.btn = 1;
        var dialogpanelhtml = "<div id='DialogPanel' style='visibility:hidden;'>";
        dialogpanelhtml += "<font id='DialogTitle'></font>";
        dialogpanelhtml += "<div id='TextDialog'></div>";
        dialogpanelhtml += "<div id='Dialogbtn0' class='btn' style='visibility:hidden;'>" + "Yes" + "</div>";
        dialogpanelhtml += "<div id='Dialogbtn1' class='btn_focus' style='visibility:hidden;'>" + "No" + "</div>";
        dialogpanelhtml += "<div id='Dialogbtn2' class='btn_focus' style='visibility:hidden;'>" + "OK" + "</div>";
        dialogpanelhtml += "</div>";

        ewf.dialogView.dialogpanel_div.innerHTML = dialogpanelhtml;
        document.body.appendChild(ewf.dialogView.dialogpanel_div);
        ewf.dialogView.DialogRead(content, 320, 180);
        if (mode == 0) {
            ewf.dialogView.time_handler = setTimeout(ewf.dialogView.DIALOG_TIME_HANDLER, time);
        }
        else if (mode == 1) {

            ewf.dialogView.btn = 2;
            $("Dialogbtn2").style.visibility = "visible";
        }
        else if (mode == 2) {
            $("Dialogbtn0").style.visibility = "visible";
            $("Dialogbtn1").style.visibility = "visible";
        }
        else if (mode == 3) {
            ewf.dialogView.hasEnterTab = true;
            $("Dialogbtn0").style.visibility = "visible";
            $("Dialogbtn1").style.visibility = "visible";
            var html = "<input type='text' id='input_email'></div>";
            var obj = document.createElement('div');
            obj.setAttribute('id', 'email');
            $('DialogPanel').appendChild(obj);
            obj.innerHTML = html;
            $('Dialogbtn' + ewf.dialogView.btn).className = "btn";
            $("input_email").focus();
        }
        ewf.dialogView.addEventListener();
    },

    DialogPanelEnd: function () {
        if (this.mode == 3) {
            ewf.Bill.email1 = $('input_email').value;
        }
        if (ewf.dialogView.time_handler != null) {
            clearTimeout(ewf.dialogView.time_handler);
        }
        if ($("DialogPanel") != null) {
            document.body.removeChild(ewf.dialogView.dialogpanel_div);
        }
        if ($('dialogpanel_background') != null)//�Ƴ�͸��
        {
            document.body.removeChild($('dialogpanel_background'));
        }
        ewf.dialogView.dialogpanel_div.innerHTML = "";
        ewf.dialogView.removeEventListener();
        if (this.callback != null) {
            this.callback();
        }
        ewf.SpecialEventHandler.SpecialEventFlag = true;
    },

    quit_to_menu: function () {
        if (this.time_handler != null) {
            clearTimeout(this.time_handler);
        }
        if ($("DialogPanel") != null) {
            document.body.removeChild(ewf.dialogView.dialogpanel_div);
        }
        ewf.dialogView.dialogpanel_div.innerHTML = "";
        if (typeof window['EONimbus'] != 'undefined') {
            Nimbus.removeEventListener(ewf.dialogView.DIALOG_EVENT_HANDLER);
            document.removeEventListener('keydown', ewf.dialogView.DIALOG_EVENT_HANDLER, false);
        }
        else
            document.removeEventListener('keydown', ewf.dialogView.DIALOG_EVENT_HANDLER, false);
    },

    DialogRead: function (content, vleft, vtop) {
        if (vleft != null && vleft != "undefined") {
            $("DialogPanel").style.left = vleft;
            if (vtop != null && vtop != "undefined")
                $("DialogPanel").style.top = vtop;
        }
        $("DialogPanel").style.visibility = "visible";

        $("TextDialog").innerHTML = content;

    },


    DIALOG_TIME_HANDLER: function () {

        ewf.dialogView.DialogPanelEnd();
    },


    onDialogLeft: function () {
//Nimbus.printOut(btn);
        if (ewf.dialogView.hasEnterTab == true) {
            $("input_email").innerHTML = $("input_email").innerHTML.substring(0, $("input_email").innerHTML.length - 1);
        }
        else {
            if (ewf.dialogView.btn > 0 && ewf.dialogView.btn < 2) {
                ewf.dialogView.btn--;
                $('Dialogbtn' + (ewf.dialogView.btn + 1)).className = "btn";
                $('Dialogbtn' + (ewf.dialogView.btn)).className = "btn_focus";
            }
        }
    },
    onDialogRight: function () {
//Nimbus.printOut("-------onright");
        if (ewf.dialogView.hasEnterTab == false) {
            if (ewf.dialogView.btn < 1) {
                ewf.dialogView.btn++;
                $('Dialogbtn' + (ewf.dialogView.btn - 1)).className = "btn";
                $('Dialogbtn' + ewf.dialogView.btn).className = "btn_focus";
            }
        }
    },

    onDialogSelect: function () {
//Nimbus.printOut("-------onselect");
        if (ewf.dialogView.hasEnterTab == false) {
            switch (ewf.dialogView.btn) {
                case 0:
                    //Nimbus.printOut("yes");
                    ewf.dialogView.setCheckoutStatus(true);
                    ewf.dialogView.DialogPanelEnd();
                    break;
                case 1:
                    //Nimbus.printOut("no");
                    ewf.dialogView.setCheckoutStatus(false);
                    ewf.dialogView.DialogPanelEnd();
                    break;
                case 2:
                    //Nimbus.printOut("CLOSE");
                    ewf.dialogView.setCheckoutStatus(true);
                    ewf.dialogView.DialogPanelEnd();
                    break;
            }
        }
    },
    DIALOG_EVENT_HANDLER: function (event) {

        if (typeof window['EONimbus'] != 'undefined') {
            if (event.EventType == Nimbus.Event.TYPE_COMMAND) {
                var Cmd = Nimbus.parseCommand(event.EventMsg);
                switch (Cmd.CommandCode) {
                    case Nimbus.Command.Left:
                        Nimbus.printOut("________left");
                        ewf.dialogView.onDialogLeft();
                        return true;
                    case Nimbus.Command.Right:
                        Nimbus.printOut("________right");
                        ewf.dialogView.onDialogRight();
                        return true;
                    case Nimbus.Command.Down:
                        Nimbus.printOut("________down");
                        ewf.dialogView.onDialogDown();
                        return true;
                    case Nimbus.Command.Up:
                        Nimbus.printOut("________up");
                        ewf.dialogView.onDialogOn();
                        return true;
                    case Nimbus.Command.Select:
                        ewf.dialogView.onDialogSelect();
                        return true;
                    case Nimbus.Command.Menu:
                        return true;
                    case Nimbus.Command.OnOff:
                        ewf.SpecialEventHandler.PowerOn();
                        return true;
                }
                return false;
            }
        }
        else {
            var e = window.event || event;
            switch (e.which) {
                case 37:
                    ewf.dialogView.onDialogLeft();
                    return true;
                case 39:
                    ewf.dialogView.onDialogRight();
                    return true;
                case 40:
                    ewf.dialogView.onDialogDown();
                    return true;
                case 38:
                    ewf.dialogView.onDialogOn();
                    return true;
                case 13:
                    ewf.dialogView.onDialogSelect();
                    return true;
            }
            // for PC mozilla and oprea
            /*if(ewf.dialogView.mode==3)
             {
             switch ( e.which ){
             case 48:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar(0));
             return true;
             case 49:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar(1));
             return true;
             case 50:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar(2));
             return true;
             case 51:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar(3));
             return true;
             case 52:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar(4));
             return true;
             case 53:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar(5));
             return true;
             case 54:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar(6));
             return true;
             case 55:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar(7));
             return true;
             case 56:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar(8));
             return true;
             case 57:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar(9));
             return true;
             case 65:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("a"));
             return true;
             case 66:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("b"));
             return true;
             case 67:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("c"));
             return true;
             case 68:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("d"));
             return true;
             case 69:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("e"));
             return true;
             case 70:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("f"));
             return true;
             case 71:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("g"));
             return true;
             case 72:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("h"));
             return true;
             case 73:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("i"));
             return true;
             case 74:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("j"));
             return true;
             case 75:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("k"));
             return true;
             case 76:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("l"));
             return true;
             case 77:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("m"));
             return true;
             case 78:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("n"));
             return true;
             case 79:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("o"));
             return true;
             case 80:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("p"));
             return true;
             case 81:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("q"));
             return true;
             case 82:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("r"));
             return true;
             case 83:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("s"));
             return true;
             case 84:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("t"));
             return true;
             case 85:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("u"));
             return true;
             case 86:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("v"));
             return true;
             case 87:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("w"));
             return true;
             case 88:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("x"));
             return true;
             case 89:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("y"));
             return true;
             case 90:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("z"));
             return true;
             case 0x6E:
             ewf.dialogView.write_email_byKeyboard(ewf.dialogView.EnterChar("."));
             return true;
             case 0x9:
             ewf.dialogView.EnterTab();
             return true;
             case 0x14:
             ewf.dialogView.EnterCapsLock();
             return true;
             }
             }*/
        }
    },


    addEventListener: function () {
        if (typeof window['EONimbus'] != 'undefined') {
            this.previousEventListener = Nimbus.EventListeners[0];
            if (this.previousEventListener != null) Nimbus.removeEventListener(this.previousEventListener);
            Nimbus.addEventListener(ewf.dialogView.DIALOG_EVENT_HANDLER);
        } else {
            this.previousEventListener = document.eventListeners[0];
            if (this.previousEventListener != null) document.removeEventListener('keydown', this.previousEventListener, false);
            document.addEventListener('keydown', ewf.dialogView.DIALOG_EVENT_HANDLER, false);
        }
    },

    removeEventListener: function () {
        if (typeof window['EONimbus'] != 'undefined') {
            Nimbus.removeEventListener(ewf.dialogView.DIALOG_EVENT_HANDLER);
            Nimbus.addEventListener(this.previousEventListener);
            this.previousEventListener = null;
        } else {
            document.removeEventListener('keydown', ewf.dialogView.DIALOG_EVENT_HANDLER, false);
            document.addEventListener('keydown', this.previousEventListener, false);
            this.previousEventListener = null;
        }
    },

    write_email_byKeyboard: function (str) {
        if (ewf.dialogView.hasEnterTab == true) {
            $("input_email").innerHTML = $("input_email").innerHTML + str;
        }
    },

    EnterTab: function () {
        if (ewf.dialogView.hasEnterTab == false) {
            ewf.dialogView.hasEnterTab == true;
        }
        else {
            ewf.dialogView.hasEnterTab == false;
        }
    },

    onDialogDown: function () {
        if (this.mode == 3) {
            if (ewf.dialogView.hasEnterTab == true) {
                ewf.dialogView.hasEnterTab = false;
                $('Dialogbtn' + ewf.dialogView.btn).className = "btn_focus";
                $("input_email").blur();
            }
        }
    },

    onDialogOn: function () {
        if (this.mode == 3) {
            if (ewf.dialogView.hasEnterTab == false) {
                ewf.dialogView.hasEnterTab = true;
                $('Dialogbtn' + ewf.dialogView.btn).className = "btn";
                $("input_email").focus();
            }
        }
    },

    EnterChar: function (str) {
        if (ewf.dialogView.hasEnterCapsLock == false) {
            return str;
        }
        else {
            return str.toUpperCase();
        }
    },

};	
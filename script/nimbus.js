/**
 * @xxxxxxxxxxxxxxxx
 * @Nimbus namespace
 * @xxxxxxxxxxxxxxxx
 **/


if (typeof this['Nimbus'] == 'undefined') {
    this['Nimbus'] = {};
    try {
        Nimbus.NimbusObj = new EONimbus;
    } catch (ex) {
        Nimbus.NimbusObj = new Object();
    }
    Nimbus.EventListeners = new Array();

    Nimbus.LOG_MSG_TYPE_APP = 0;
    Nimbus.LOG_MSG_TYPE_API_DISABLED = 1;
    Nimbus.LOG_MSG_TYPE_API_ALWAYS = 2;
    Nimbus.LOG_MSG_TYPE_API_ERROR = 3;
    Nimbus.LOG_MSG_TYPE_API_DEBUG = 4;
}


/**
 * @xxxxxxxxxxxxxxxx
 * @Nimbus common API
 * @xxxxxxxxxxxxxxxx
 **/



Nimbus.printOut = function (msg) {
    Nimbus.NimbusObj.LogDebugMessage(Nimbus.LOG_MSG_TYPE_APP, msg + '\n');
}


/**
 * @xxxxxxxxxxxxxxxx
 * @Nimbus  global API
 * @xxxxxxxxxxxxxxxx
 **/



Nimbus.getHardwareModel = function () {
    return Nimbus.NimbusObj.HardwareModel;
}

Nimbus.getHardwareID = function () {
    return Nimbus.NimbusObj.HardwareID;
}

Nimbus.getEPLDRevision = function () {
    return Nimbus.NimbusObj.EPLDRevision;
}

Nimbus.getFirmwareRevision = function () {
    return Nimbus.NimbusObj.FirmwareRevision;
}

Nimbus.getSerialNumber = function () {
    return Nimbus.NimbusObj.SerialNumber;
}


/**
 * @xxxxxxxxxxxxxxxx
 * @Nimbus alter API
 * @xxxxxxxxxxxxxxxx
 **/



Nimbus.handShake = function () {
    return Nimbus.NimbusObj.Handshake();
}

Nimbus.reboot = function () {
    return Nimbus.NimbusObj.Reboot();
}

Nimbus.reload = function () {
    return Nimbus.NimbusObj.Reload();
}

Nimbus.setRFAutoUpdateParameters = function (RFChannel, bSearch) {
    return Nimbus.NimbusObj.SetRFAutoUpdateParameters(RFChannel, bSearch);
}

Nimbus.updateFirmware = function (url) {
    return Nimbus.NimbusObj.UpdateFirmware(url);
}

Nimbus.updateContent = function (Location, Port, TransferProtocol, Username, Password, UpdateFile, UpdateFolder, UpdateDrive) {
    return Nimbus.NimbusObj.UpdateContent(Location, Port, TransferProtocol, Username, Password, UpdateFile, UpdateFolder, UpdateDrive);
}

Nimbus.updateConfiguration = function (url) {
}

Nimbus.restoreFactoryConfiguration = function () {
    return Nimbus.NimbusObj.RestoreFactoryConfiguration();
}

Nimbus.getGraphicsResolution = function () {
    var res = new Array();
    res.width = Nimbus.NimbusObj.GetGraphicsResolutionWidth();
    res.height = Nimbus.NimbusObj.GetGraphicsResolutionHeight();
    return res;
}

Nimbus.setGraphicsResolution = function (width, height) {
    return Nimbus.NimbusObj.SetGraphicsResolution(width, height);
}


/**
 * @xxxxxxxxxxxxxxxx
 * @Nimbus user API
 * @xxxxxxxxxxxxxxxx
 **/



Nimbus.getMousePointerEnable = function () {
    return Nimbus.NimbusObj.GetMousePointerEnable();
}

Nimbus.setMousePointerEnable = function (state) {
    Nimbus.NimbusObj.SetMousePointerEnable(state);
    return true;
}

Nimbus.getAutoSpacialNavigationEnable = function () {
    return Nimbus.NimbusObj.GetAutoSpacialNavigation();
}

Nimbus.setAutoSpacialNavigationEnable = function (state) {
    Nimbus.NimbusObj.SetAutoSpacialNavigation(state);
    return true;
}

Nimbus.displayMessage = function (msg, scale) {
    Nimbus.NimbusObj.DisplayMessage(msg, scale);
    return true;
}

Nimbus.showOptionMenu = function () {
    Nimbus.NimbusObj.ShowOptionMenu();
    return true;
}

Nimbus.clearNativeGUI = function () {
    Nimbus.NimbusObj.ClearNativeGUI();
    return true;
}

Nimbus.getCCVisibility = function () {
    return Nimbus.NimbusObj.GetCCVisibility();
}

Nimbus.setCCVisibility = function (state) {
    return Nimbus.NimbusObj.SetCCVisibility(state);
}

Nimbus.getCCMode = function () {
    return Nimbus.NimbusObj.GetCCMode();
}

Nimbus.setCCMode = function (state) {
    return Nimbus.NimbusObj.SetCCMode(state);
}

Nimbus.getAnalogCCMode = function () {
    return Nimbus.NimbusObj.GetAnalogCCMode();
}

Nimbus.setAnalogCCMode = function (mode) {
    return Nimbus.NimbusObj.SetAnalogCCMode(mode);
}

Nimbus.getDigitalCCMode = function () {
    return Nimbus.NimbusObj.GetDigitalCCMode();
}

Nimbus.setDigitalCCMode = function (mode) {
    return Nimbus.NimbusObj.SetDigitalCCMode(mode);
}

Nimbus.resetCC = function () {
    return Nimbus.NimbusObj.ResetCC();
}

Nimbus.getVChipRatingPopupEnable = function () {
    return Nimbus.NimbusObj.GetVChipRatingPopupEnable();
}

Nimbus.setVChipRatingPopupEnable = function (state) {
    return Nimbus.NimbusObj.SetVChipRatingPopupEnable(state);
}

Nimbus.allowVChipBlockedProgram = function () {
    return Nimbus.NimbusObj.AllowVChipBlockedProgram();
}

Nimbus.resetVChip = function () {
    return Nimbus.NimbusObj.ResetVChip();
}

Nimbus.setSignalStatusPopupEnable = function (state) {
    return Nimbus.NimbusObj.SetSignalStatusPopupEnable(state);
}

Nimbus.getSignalStatusPopupEnable = function () {
    return Nimbus.NimbusObj.GetSignalStatusPopupEnable();
}


/**
 * @xxxxxxxxxxxxxxxx
 * @Nimbus output API
 * @xxxxxxxxxxxxxxxx
 **/



Nimbus.isOutputActive = function () {
    return Nimbus.NimbusObj.GetOutputActive();
}

Nimbus.getOutputProtection = function () {
    return Nimbus.NimbusObj.GetOutputProtection();
}

Nimbus.setOutputProtection = function (state) {
}

Nimbus.execShellCommand = function (cmd) {
}


/**
 * @xxxxxxxxxxxxxxxx
 * @Nimbus network API
 * @nodebug
 * @xxxxxxxxxxxxxxxx
 **/



Nimbus.ping = function () {
}

Nimbus.getNetworkDeviceList = function () {
    var count = Nimbus.NimbusObj.NetworkDeviceCount;
    var devicelist = new Array();
    for (var i = 0; i < count; i++) {
        devicelist[i] = Nimbus.NimbusObj.GetNetworkDeviceName(i);
    }
    return devicelist;
}

Nimbus.getNetworkDevice = function (deviceID) {
    return new Nimbus.NetworkDevice(deviceID);
}

// New Object --- EONimbusNetworkDevice()

Nimbus.NetworkDevice = function (deviceID) {
    this.netObj = new EONimbusNetworkDevice(deviceID);
}

Nimbus.NetworkDevice.prototype.getEnable = function () {
    if (this.netObj == null) return null;
    return this.netObj.GetEnable();
}

Nimbus.NetworkDevice.prototype.setEnable = function (state) {
    if (this.netObj == null) return null;
    return this.netObj.SetEnable(state);
}

Nimbus.NetworkDevice.prototype.getLinkType = function () {
    if (this.netObj == null) return 'UNKNOWN';
    return this.netObj.Type;
}

Nimbus.NetworkDevice.prototype.getMACAddress = function () {
    if (this.netObj == null) return null;
    return this.netObj.GetMACAddress();
}

Nimbus.NetworkDevice.prototype.getIPAddress = function () {
    if (this.netObj == null) return null;
    return this.netObj.GetIPAddress();
}

Nimbus.NetworkDevice.prototype.getNetmask = function () {
    if (this.netObj == null) return null;
    return this.netObj.GetNetmask();
}

Nimbus.NetworkDevice.prototype.getGateway = function () {
    if (this.netObj == null) return null;
    return this.netObj.GetGateway();
}

Nimbus.NetworkDevice.prototype.isVLANSupported = function () {
    if (this.netObj == null) return null;
    return this.netObj.GetVLANSupport();
}

Nimbus.NetworkDevice.prototype.getVLANTaggingEnable = function () {
    if (this.netObj == null) return null;
    return this.netObj.GetVLANTaggingEnable();
}

Nimbus.NetworkDevice.prototype.setVLANTaggingEnable = function (state) {
    if (this.netObj == null) return null;
    return this.netObj.setVLANTaggingEnable(state);
}

Nimbus.NetworkDevice.prototype.getVLANTag = function () {
    if (this.netObj == null) return null;
    return this.netObj.GetVLANTag();
}

Nimbus.NetworkDevice.prototype.setVLANTag = function (id) {
    if (this.netObj == null) return null;
    return this.netObj.setVLANTag(id);
}


/**
 * @xxxxxxxxxxxxxxxx
 * @Nimbus player API
 * @nodebug
 * @xxxxxxxxxxxxxxxx
 **/



Nimbus.getPlayer = function (url, position, sessionID) {
    if (url == null) return Nimbus.CurrentPlayer;
    if (Nimbus.CurrentPlayer != null) return null;
    var initPos = 0;
    if (url != null) initPos = position;
    var initSes = '';
    if (sessionID != null) initSes = sessionID;
    var player = new Nimbus.Player(url, initPos, initSes);
    if (player != null) Nimbus.CurrentPlayer = player;
    return player;
}

// New Object --- EONimbusPlayer()

Nimbus.Player = function (url, position, sessionID) {
    this.playerObj = new EONimbusPlayer(url, position, sessionID);
    this.url = url;
}

Nimbus.Player.prototype.setContentURL = function (url, sessionID) {
    if (this.playerObj == null) return false;
    this.url = url;
    var initSes = '';
    if (sessionID != null) initSes = sessionID;
    return this.playerObj.SetContent(this.url, initSes);
}

Nimbus.Player.prototype.isReady = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetReadyStatus();
}

Nimbus.Player.prototype.isError = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetErrorStatus();
}

Nimbus.Player.prototype.isContentDisplayable = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.IsContentDisplayable();
}

Nimbus.Player.prototype.isVChipBlocking = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetVChipBlocking();
}

Nimbus.Player.prototype.getErrorDesc = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetErrorDesc();
}

Nimbus.Player.prototype.getProgramRating = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetProgramRating();
}

Nimbus.Player.prototype.getContentTitle = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetContentTitle();
}

Nimbus.Player.prototype.getContentLength = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetContentLength();
}

Nimbus.Player.prototype.getSpeed = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetSpeed();
}

Nimbus.Player.prototype.setSpeed = function (rate) {
    if (this.playerObj == null) return false;
    return this.playerObj.SetSpeed(rate);
}

Nimbus.Player.prototype.play = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.Play();
}

Nimbus.Player.prototype.pause = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.Pause();
}

Nimbus.Player.prototype.stop = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.Stop();
}

Nimbus.Player.prototype.close = function () {
    if (this.playerObj == null) return false;
    Nimbus.CurrentPlayer = null;
    return this.playerObj.Close();
}

Nimbus.Player.prototype.getPosition = function () {
    if (this.playerObj == null) return null;
    return this.playerObj.GetPosition();
}

Nimbus.Player.prototype.setPosition = function (position) {
    if (this.playerObj == null) return false;
    return this.playerObj.SetPosition(position);
}

Nimbus.Player.prototype.getRetryEnable = function () {
    if (this.playerObj == null) return null;
    return this.playerObj.GetRetryEnable();
}

Nimbus.Player.prototype.setRetryEnable = function (state) {
    if (this.playerObj == null) return false;
    return this.playerObj.SetRetryEnable(state);
}

Nimbus.Player.prototype.getVideoLayerEnable = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetVideoLayerEnable();
}

Nimbus.Player.prototype.setVideoLayerEnable = function (state) {
    if (this.playerObj == null) return false;
    return this.playerObj.SetVideoLayerEnable(state);
}

Nimbus.Player.prototype.getVideoLayerRect = function () {
    if (this.playerObj == null) return false;
    var rect = new Array();
    rect.x = this.playerObj.GetVideoLayerRectX();
    rect.y = this.playerObj.GetVideoLayerRectY();
    rect.width = this.playerObj.GetVideoLayerRectWidth();
    rect.height = this.playerObj.GetVideoLayerRectHeight();
    return rect;
}

Nimbus.Player.prototype.setVideoLayerRect = function (x, y, w, h) {
    if (this.playerObj == null) return false;
    return this.playerObj.SetVideoLayerRect(x, y, w, h);
}

Nimbus.Player.prototype.getVideoLayerBlendingMode = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetVideoLayerBlendingMode();
}

Nimbus.Player.prototype.setVideoLayerBlendingMode = function (mode) {
    if (this.playerObj == null) return false;
    return this.playerObj.SetVideoLayerBlendingMode(mode);
}

Nimbus.Player.prototype.getVideoLayerTransparency = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetVideoTransparency();
}

Nimbus.Player.prototype.setVideoLayerTransparency = function (transparency) {
    if (this.playerObj == null) return false;
    return this.playerObj.SetVideoTransparency(transparency);
}

Nimbus.Player.prototype.getChromaKeyColor = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetVideoChromaKey();
}

Nimbus.Player.prototype.setChromaKeyColor = function (color) {
    if (this.playerObj == null) return false;
    return this.playerObj.SetVideoChromaKey(color);
}

Nimbus.Player.prototype.getPictureFormat = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetPictureFormat();
}

Nimbus.Player.prototype.setPictureFormat = function (mode) {
    if (this.playerObj == null) return false;
    return this.playerObj.SetPictureFormat(mode);
}

Nimbus.Player.prototype.getPrimaryCCSource = function () {
    return true;
}

Nimbus.Player.prototype.setPrimaryCCSource = function (state) {
    return true;
}

Nimbus.Player.prototype.getVolume = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetVolume();
}

Nimbus.Player.prototype.setVolume = function (volume) {
    if (this.playerObj == null) return false;
    return this.playerObj.SetVolume(volume);
}

Nimbus.Player.prototype.sendRTSPGetParameter = function (request) {
    if (this.playerObj == null) return false;
    return this.playerObj.SendRTSPGetParameter(request);
}

Nimbus.Player.prototype.sendRTSPSetParameter = function (request) {
    if (this.playerObj == null) return false;
    return this.playerObj.SendRTSPSetParameter(request);
}

Nimbus.Player.prototype.getRTSPResponse = function (handle) {
    if (this.playerObj == null) return false;
    return this.playerObj.GetRTSPResponse(handle);
}

Nimbus.Player.prototype.getRTSPAnnouncement = function () {
    if (this.playerObj == null) return false;
    return this.playerObj.GetRTSPAnnouncement();
}


/**
 * @xxxxxxxxxxxxxxxx
 * @Nimbus TV API
 * @nodebug
 * @xxxxxxxxxxxxxxxx
 **/



Nimbus.getTVController = function () {
    // Return the current controller, if any.
    if (Nimbus.CurrentController != null) {
        Nimbus.NimbusObj.LogDebugMessage(Nimbus.LOG_MSG_TYPE_API_DEBUG, "Nimbus.getTVController, returning existing object");
        return Nimbus.CurrentController;
    }
    // Create a new TV controller class
    var controller = new Nimbus.TVController();
    if (controller != null) {
        Nimbus.CurrentController = controller;
    }
    return controller;
};

/**
 * Interface for controlling a TV.
 *
 * @class
 */

Nimbus.TVController = function () {
    // Create a native js player object
    this.TVCtrl = new EONimbusTVController();
    Nimbus.NimbusObj.LogDebugMessage(Nimbus.LOG_MSG_TYPE_API_DEBUG, "Nimbus.TVController constructor");
};

/**
 * Gets the number of available TV A/V inputs.
 *
 * <br><br><b>Unimplemented</b>
 *
 * @return {Number} The total number of available A/V inputs
 */

Nimbus.TVController.prototype.getInputCount = function () {
    if (this.TVCtrl == null) {
        return null;
    }
    return this.TVCtrl.GetInputCount();
};

/**
 * Gets a list of names describing the TV A/V inputs.  The order is the same as that used by the setInput function.
 *
 * <br><br><b>Unimplemented</b>
 *
 * @return {Array} An array of names (strings) describing the A/V inputs.
 */

Nimbus.TVController.prototype.getInputNames = function () {
    if (this.TVCtrl == null) {
        return null;
    }
};

/**
 * Sets the power state of the TV.
 *
 * @param {Boolean} state  True if power should be enabled
 * @return {Boolean} True if successful
 */

Nimbus.TVController.prototype.setPower = function (state) {
    if (this.TVCtrl == null) {
        return false;
    }
    return this.TVCtrl.Power = state;
};

/**
 * Gets the power state of the TV.
 *
 * @return {Boolean} True if TV power is enabled or null if power state is unknown
 */

Nimbus.TVController.prototype.getPower = function () {
    if (this.TVCtrl == null) {
        return null;
    }
    return this.TVCtrl.Power;
};

/**
 * Sets the TV A/V input to use.  Index 0 always corresponds to the STB/IMP A/V output.
 *
 * <br><br><b>Unimplemented</b>
 *
 * @param {Number} index A number from 0 to TVController.getInputCount()-1
 *
 * @return {Boolean} True if successful. False if the input is not available on this TV.
 */

Nimbus.TVController.prototype.setInput = function (index) {
    if (this.TVCtrl == null) {
        return null;
    }
    return this.TVCtrl.SetInput(index);
};

/**
 * Gets the TV A/V input currently selected.
 *
 * <br><br><b>Unimplemented</b>
 *
 * @return {Number} A number from 0 to TVController.getInputCount()-1, or null if input selection is unknown
 */

Nimbus.TVController.prototype.getInput = function () {
    if (this.TVCtrl == null) {
        return null;
    }
    return this.TVCtrl.GetInput();
};

/**
 * Sets the audio volume level of the TV.
 *
 * @param {Number} volume A number from 0 to 100, with 0 being muted and 100 being full volume
 * @return {Boolean} True if successful
 */

Nimbus.TVController.prototype.setVolume = function (volume) {
    if (this.TVCtrl == null) {
        return null;
    }
    return this.TVCtrl.Volume = volume;
};

/**
 * Gets the audio volume level of the TV.
 *
 * @return {Number} A number from 0 to 100, with 0 being muted and 100 being full volume
 */

Nimbus.TVController.prototype.getVolume = function () {
    if (this.TVCtrl == null) {
        return null;
    }
    return this.TVCtrl.Volume;
};

/**
 * Set the mute state of the TV audio output.
 *
 * @param {Boolean} state True to mute the audio output
 * @return {Boolean} True if successful
 */

Nimbus.TVController.prototype.setMute = function (state) {
    if (this.TVCtrl == null) {
        return null;
    }
    return this.TVCtrl.Mute = state;
};

/**
 * Gets the mute state of the TV audio output.
 *
 * @return {Boolean} True if TV is muted, or null if undeterminable
 */

Nimbus.TVController.prototype.getMute = function () {
    if (this.TVCtrl == null) {
        return null;
    }
    return this.TVCtrl.Mute;
};

/**
 * Sets the enable state of the volume indicator display for presenting the volume
 * level when volume commands are processed.
 *
 * <br><br><b>Unimplemented</b>
 *
 * @param {Boolean} state True to display the volume indicator
 * @return {Boolean} True if successful
 */

Nimbus.TVController.prototype.setVolumeIndicatorEnable = function (state) {
    if (this.TVCtrl == null) {
        return null;
    }
    return this.TVCtrl.SetVolumeIndicatorEnable(state);
};

/**
 * Gets the enable state of the volume indicator.
 *
 * <br><br><b>Unimplemented</b>
 *
 * @return {Boolean} True if the volume indicator is used
 */

Nimbus.TVController.prototype.getVolumeIndicatorEnable = function () {
    if (this.TVCtrl == null) {
        return null;
    }
    return this.TVCtrl.GetVolumeIndicatorEnable();
};


/**
 * @xxxxxxxxxxxxxxxx
 * @Nimbus event API
 * @nodebug
 * @xxxxxxxxxxxxxxxx
 **/



Nimbus.Event = function (eType, eMsg) {
    this.EventType = eType;
    this.EventMsg = eMsg;
}

Nimbus.Event.TYPE_INVALID = 0;
Nimbus.Event.TYPE_COMMAND = 1;
Nimbus.Event.TYPE_NOTIFICATION = 2;

Nimbus.eventCallback = function (eStr) {
    var bProcessed = false;
    var event = new Nimbus.Event(0, '');
    var pos = eStr.search(/,/i);
    if (pos != -1) {
        event.EventType = parseInt(eStr.slice(0, pos));
        event.EventMsg = eStr.slice(pos + 1, eStr.length);
        bProcessed = Nimbus.dispatchEvent(event);
        if (!bProcessed && event.EventType == Nimbus.Event.TYPE_COMMAND) Nimbus.NimbusObj.SendCommand(event.EventMsg);
    }
    return bProcessed;
}

Nimbus.dispatchEvent = function (el) {
    for (var i = 0; i < Nimbus.EventListeners.length; i++) {
        if (Nimbus.EventListeners[i] != null && Nimbus.EventListeners[i](el)) return true;
    }
    return false;
}

Nimbus.addEventListener = function (el) {
    for (var i = 0; i < Nimbus.EventListeners.length; i++) {
        if (Nimbus.EventListeners[i] == el) return true;
    }
    if (el == ewf.SpecialEventHandler.SpecialEventListener) Nimbus.EventListeners[1] = el;
    else Nimbus.EventListeners[0] = el;
    return true;
}

Nimbus.removeEventListener = function (el) {
    for (var i = 0; i < Nimbus.EventListeners.length; i++) {
        if (Nimbus.EventListeners[i] == el) {
            Nimbus.EventListeners[0] = null;
            return true;
        }
    }
    return false;
}

Nimbus.Command = function (cCode, cSource) {
    this.CommandCode = cCode;
    this.CommandSource = cSource;
}

Nimbus.parseCommand = function (cStr) {
    var Cmd = new Nimbus.Command(0, 0);
    var pos = cStr.search(/,/i);
    if (pos != -1) {
        Cmd.CommandCode = parseInt(cStr.slice(0, pos));
        Cmd.CommandSource = cStr.slice(pos + 1, cStr.length);
        return Cmd;
    }
    return null;
}

Nimbus.reloadPage = function () {
    var TVObj = Nimbus.getTVController();
    if (!TVObj.getPower()) {
        Nimbus.reload();
        Nimbus.printOut('Reload this page...');
    }
}

//
// Command codes
//
Nimbus.Command.Base = 0xf000;
Nimbus.Command.OnOff = Nimbus.Command.Base + 0x01;	// On/off toggle
Nimbus.Command.On = Nimbus.Command.Base + 0x43;	// On
Nimbus.Command.Off = Nimbus.Command.Base + 0x44;	// Off

Nimbus.Command.Up = Nimbus.Command.Base + 0x02;	// Nav up
Nimbus.Command.Down = Nimbus.Command.Base + 0x03;	// Nav down
Nimbus.Command.Left = Nimbus.Command.Base + 0x04; 	// Nav left
Nimbus.Command.Right = Nimbus.Command.Base + 0x05;	// Nav right
Nimbus.Command.Select = Nimbus.Command.Base + 0x06;	// Nav select

Nimbus.Command.Menu = Nimbus.Command.Base + 0x07;	// Menu

Nimbus.Command.VolPlus = Nimbus.Command.Base + 0x08;	// Volume plus
Nimbus.Command.VolMinus = Nimbus.Command.Base + 0x09;	// Volume minus
Nimbus.Command.Mute = Nimbus.Command.Base + 0x0b;	// Mute

Nimbus.Command.Key1 = Nimbus.Command.Base + 0x0e;	// Digit keys
Nimbus.Command.Key2 = Nimbus.Command.Base + 0x0f;
Nimbus.Command.Key3 = Nimbus.Command.Base + 0x10;
Nimbus.Command.Key4 = Nimbus.Command.Base + 0x11;
Nimbus.Command.Key5 = Nimbus.Command.Base + 0x12;
Nimbus.Command.Key6 = Nimbus.Command.Base + 0x13;
Nimbus.Command.Key7 = Nimbus.Command.Base + 0x14;
Nimbus.Command.Key8 = Nimbus.Command.Base + 0x15;
Nimbus.Command.Key9 = Nimbus.Command.Base + 0x16;
Nimbus.Command.Key0 = Nimbus.Command.Base + 0x17;

Nimbus.Command.PlayPause = Nimbus.Command.Base + 0x18;	// Play/pause
Nimbus.Command.Stop = Nimbus.Command.Base + 0x19;	// Stop
Nimbus.Command.FastRewind = Nimbus.Command.Base + 0x1b;	// Rewind
Nimbus.Command.FastForward = Nimbus.Command.Base + 0x1c;	// Fast forward

Nimbus.Command.ChanPlus = Nimbus.Command.Base + 0x2b;	// Channel plus
Nimbus.Command.ChanMinus = Nimbus.Command.Base + 0x2c;	// Channel minus
Nimbus.Command.PrevChan = Nimbus.Command.Base + 0x2d;	// Previous channel viewed

Nimbus.Command.Sleep = Nimbus.Command.Base + 0x2e;	// Sleep mode selection
Nimbus.Command.SleepAlt = Nimbus.Command.Base + 0x30;	// Sleep mode selection
Nimbus.Command.ScreenFormatAlt = Nimbus.Command.Base + 0x35; 	// Screen format selection
Nimbus.Command.CCAlt = Nimbus.Command.Base + 0x55;	// CC control
Nimbus.Command.SAP = Nimbus.Command.Base + 0x4e;	// Secondary audio program

Nimbus.Command.DisplayLabel = Nimbus.Command.Base + 0x4c;	// Display channel label
Nimbus.Command.Status = Nimbus.Command.Base + 0x2a;	// Display status screen

Nimbus.Command.SelectComposite = Nimbus.Command.Base + 0x73;	// Select composite input
Nimbus.Command.SelectVGA = Nimbus.Command.Base + 0x76;	// Select VGA input
Nimbus.Command.SelectHDMI = Nimbus.Command.Base + 0x77;	// Select HDMI input

Nimbus.Command.AppSpecificA = Nimbus.Command.Base + 0x56;	// Special function keys with application-specific purposes
Nimbus.Command.AppSpecificB = Nimbus.Command.Base + 0x57;	//   such as for use by Javascript-based VOD applications
Nimbus.Command.AppSpecificC = Nimbus.Command.Base + 0x58;
Nimbus.Command.AppSpecificD = Nimbus.Command.Base + 0x59;
Nimbus.Command.AppSpecificE = Nimbus.Command.Base + 0x5a;
Nimbus.Command.AppSpecificF = Nimbus.Command.Base + 0x5b;
Nimbus.Command.AppSpecificG = Nimbus.Command.Base + 0x5c;
Nimbus.Command.AppSpecificH = Nimbus.Command.Base + 0x5d;
Nimbus.Command.AppSpecificI = Nimbus.Command.Base + 0x5e;
Nimbus.Command.AppSpecificJ = Nimbus.Command.Base + 0x5f;


/**
 * @Nimbus End
 **/
/**********************************************************************************************
 ***********************************************************************************************
 **********************************************************************************************/

Nimbus.getSettingsXml = function () {
    return Nimbus.NimbusObj.GetSettingsXml();
};
Nimbus.setSettingsXml = function (xml, applyAgainstDefaults) {
    return Nimbus.NimbusObj.SetSettingsXml(xml, applyAgainstDefaults);
};
Nimbus.logMessage = function (msg) {
    Nimbus.NimbusObj.LogDebugMessage(Nimbus.LOG_MSG_TYPE_APP, msg + '\n');
};
Nimbus.setGraphicsUnderscan = function (underscan) {
    return Nimbus.NimbusObj.SetGraphicsUnderscan(underscan);
};
Nimbus.getGraphicsUnderscan = function () {
    return Nimbus.NimbusObj.GetGraphicsUnderscan();
};

Nimbus.setIPAutoUpdateParameters = function (bEnable, IPAddress, IPPort) {
    if (IPAddress == null) {
        return Nimbus.NimbusObj.SetIPAutoUpdateParameters(bEnable);
    } else {
        return Nimbus.NimbusObj.SetIPAutoUpdateParameters(bEnable, IPAddress, IPPort);
    }
};

if (typeof window['EONimbus'] != 'undefined') {
    //SetTVSettings();
    Nimbus.setIPAutoUpdateParameters(false, "239.79.200.175", 20000);

    var handShakeTimerCount = 0;
    var handShakeTimer = function () {
        Nimbus.handShake();
        handShakeTimerCount++;
        if (handShakeTimerCount <= 30) {
            handShakeTimeout = setTimeout(handShakeTimer, 2000);
        } else {
            handShakeTimerCount = 0;
        }
        Nimbus.printOut('============Nimbus HandShake has finished...==============');
    }
    var TVObj = Nimbus.getTVController();
    TVObj.setVolumeIndicatorEnable(false);
    TVObj.setVolume(25);
    if (!TVObj.getPower()) setTimeout('Nimbus.reloadPage();', 2 * 60 * 60 * 1000);
    Nimbus.setAutoSpacialNavigationEnable(false);
    Nimbus.setMousePointerEnable(false);
    Nimbus.setSignalStatusPopupEnable(false);
    Nimbus.setOutputProtection(true);
    Nimbus.resetCC();
    var handShakeTimeout = setTimeout(handShakeTimer, 2000);
    Nimbus.printOut('Nimbus initialize finished...');
}
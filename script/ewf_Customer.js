/**
 * @customer
 **/

ewf.Customer = {
    init: function () {
        this.device_id = this.get_device();
        var url = '../stbservlet?attribute=ewf_get_user_data&device_id=' + this.device_id;
        this.send(url);
    },

    send: function (url) {
        var obj = new Ajax(url, this.resp, null, 0);
        if (obj != null) {
            obj.get();
        }
    },

    resp: function (response, param) {
       try {
            eval( 'var hash_customer_obj = ' + response + ';' );
            ewf.Customer.userData = hash_customer_obj;
            if ( typeof window['EONimbus'] != 'undefined' ){
                Nimbus.printOut('--- Customer Information loaded ---');
            }
            var _p = location.href.split("?key=");
            if (_p.length != 1 &&  _p[1] == "0" ){
                ewf.ProvisionModule.callOperatorLogin();
                ewf.ProvisionModule.isReProvision = true;
                return false;
            }
        } catch(ex){
            ewf.ProvisionModule.callOperatorLogin();
            return false;
        }
        ewf.Customer.judgePortal();
    },

    get_device: function () {
        if (window.platform == 'ENSEO') {
            var device_list = Nimbus.getNetworkDeviceList();
            var username = device_list[0];
            var device = Nimbus.getNetworkDevice(username);
            var mac = device.getMACAddress().replace(/:/gi, '');
        }
        else if (window.platform == 'NEBULA') {
            var device_list = Nebula.getNetworkDeviceList();
            var username = device_list[0];
            var device = Nebula.getNetworkDevice(username);
            var mac = device.getMAC().replace(/:/gi, '');
        }
        else if (window.platform == 'LG') {
            var mac = window.LG.deviceId;
        }
        else {
            var mac = '000000000000';
        }
        return mac;
    },

    judgePortal: function () {
        var portalId = this.userData.portalID;
        var postfix = '';

        for (var i = 0; i < ewf.conf.tvLocations.length; i++) {
            if (ewf.conf.tvLocations[i].value.toLowerCase() == this.userData['TVNumber'].toLowerCase()) {
                postfix = ewf.conf.tvLocations[i].portalPostfix;
                break;
            }
        }

        if (portalId) {
            var url = "/ewf_" + portalId;
            if(postfix)
                url += postfix;
            debug('About to redirect to "' + url + '"');
            location.href = url;
        } else {
            return false;
        }
    }
};

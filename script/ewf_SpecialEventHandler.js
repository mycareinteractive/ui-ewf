﻿ewf.SpecialEventHandler = {
    ComplexKey: 0,
    select_count: 0,
    SpecialEventFlag: true,
    reloadPage_Timeout: null,
    lastPowerState: null,

    SpecialEventListener: function (event) {
        if (ewf.SpecialEventHandler.SpecialEventFlag == true) {
            if (typeof window['EONimbus'] != 'undefined') {
                if (event.EventType == Nimbus.Event.TYPE_COMMAND) {
                    if (plugin_SS.active) {
                        plugin_SS.destroy();
                        plugin_SS.active = false;
                    } else {
                        plugin_SS.standbyLimit = 0;
                    }
                    if (ewf.DataCenter.Customer.checkout == false) {
                        var Cmd = Nimbus.parseCommand(event.EventMsg);
                        switch (Cmd.CommandCode) {
                            case Nimbus.Command.SelectComposite:
                                ewf.SpecialEventHandler.goto_AVInput();
                                return true;
                            case Nimbus.Command.SelectVGA:
                                ewf.SpecialEventHandler.goto_VGAInput();
                                return true;
                            case Nimbus.Command.SelectHDMI:
                                ewf.SpecialEventHandler.goto_HDMIInput();
                                return true;
                            case Nimbus.Command.AppSpecificB:
                                ewf.SpecialEventHandler.goto_WynnServices();
                                return true;
                            case Nimbus.Command.AppSpecificC:
                                ewf.SpecialEventHandler.goto_Movies();
                                return true;
                            case Nimbus.Command.AppSpecificD:
                                ewf.SpecialEventHandler.goto_Languages();
                                return true;
                            case Nimbus.Command.AppSpecificE:
                                ewf.SpecialEventHandler.goto_MusicMoods();
                                return true;
                            case 61493:
                                ewf.SpecialEventHandler.goto_Internet();
                                return false;
                            case Nimbus.Command.AppSpecificG:
                                ewf.SpecialEventHandler.goto_ProgramGuide();
                                return true;
                            case Nimbus.Command.AppSpecificH:
                                ewf.SpecialEventHandler.goto_LocalTelevision();
                                return true;
                            case 61534:
                                return true;
                            case Nimbus.Command.AppSpecificJ:
                                ewf.SpecialEventHandler.goto_HotelBill();
                                return false;
                            case 61525:
                                return false;
                            case Nimbus.Command.AppSpecificA:
                                ewf.SpecialEventHandler.goto_Help();
                                return true;
                            case 61488:
                                return false;
                            case Nimbus.Command.ChanMinus:
                            case Nimbus.Command.ChanPlus:
                                if ($('ColourfulIcon') != null) {
                                    ewf.SpecialEventHandler.goto_LocalTelevision();
                                }
                                return true;
                            case Nimbus.Command.OnOff:
                                ewf.SpecialEventHandler.PowerOn();
                                return true;
                        }
                    }
                    return false;
                }
            }
            if ($("ColourfulIcon") != null) {
                var e = window.event || event;
                switch (e.which) {
                    case 49:
                        ewf.SpecialEventHandler.show(1);
                        return true;
                    case 50:
                        ewf.SpecialEventHandler.show(2);
                        return true;
                    case 51:
                        ewf.SpecialEventHandler.show(3);
                        return true;
                    case 52:
                        ewf.SpecialEventHandler.show(4);
                        return true;
                    case 53:
                        ewf.SpecialEventHandler.show(5);
                        return true;
                    case 54:
                        ewf.SpecialEventHandler.show(6);
                        return true;
                    case 55:
                        ewf.SpecialEventHandler.show(7);
                        return true;
                    case 56:
                        ewf.SpecialEventHandler.show(8);
                        return true;
                    case 57:
                        ewf.SpecialEventHandler.show(9);
                        return true;
                    case 48:
                        ewf.SpecialEventHandler.show(0);
                        return true;
                }
            }
        }
    },

    removeAll_and_Init: function () {
        // mingxiang 20090612
        // internet
        if (ewf.InternetModule.BrowserWin != null) {
            ewf.InternetModule.BrowserWin.destroy();
            Nimbus.setAutoSpacialNavigationEnable(false);
            Nimbus.setMousePointerEnable(false);
            window.focus();
        }
        if (ewf.InternetModule.internetTimeout != null) {
            clearTimeout(ewf.InternetModule.internetTimeout);
            clearInterval(ewf.InternetModule.internetURLTimer);
            ewf.InternetModule.internetTimeout = null;
            ewf.InternetModule.internetURLTimer = null;
        }
        // mingxiang end

        if ($("StreamPanel") != null) {
            if (typeof window['EONimbus'] != 'undefined') {
                var player = Nimbus.getPlayer();
                var position = player.getPosition();
                if (player.isError() == 0) {
                    var url = '../stbservlet?attribute=ote_update_ticket_suspend_position&home_id='
                        + ewf.DataCenter.Customer.home_id + '&ticket_id=' + ewf.RTSPStream.ticket_id + '&suspend_position=' + position;
                    var obj = new Ajax(url, null, null, 0);
                    if (obj != null) {
                        obj.get();
                    }
                }
                player.close();
            }
            clearTimeout(ewf.RTSPStream.Description_timeout);
            clearTimeout(ewf.RTSPStream.timeout);
            if (ewf.RTSPStream.isPlayOK_timeout != null) {
                clearTimeout(ewf.RTSPStream.isPlayOK_timeout);
            }
            if (ewf.RTSPStream.GracePeriodTimer != null) {
                clearTimeout(ewf.RTSPStream.GracePeriodTimer);
            }
        }
        if (typeof window['EONimbus'] != 'undefined') {
            var player = Nimbus.getPlayer();
            if (player != null) {
                player.close();
            }
        }
        clearTimeout(ewf.Platform.clock_timeout);
        if (typeof window['EONimbus'] != 'undefined') {
            Nimbus.EventListeners[0] = null;
        }
        else {
            this.previousEvent = document.eventListeners[0];
            document.removeEventListener('keydown', this.previousEvent, false);
            document.eventListeners[0] = null;
        }
        document.onkeydown = null;
        if ($('ChannelPanel') != null) {
            document.removeEventListener('keydown', ewf.ChannelGroup.channel_listener, false);
        }
        document.body.innerHTML = '';
        ewf.DataCenter.from_SpecialEvent();
        plugin_SS.streaming = false;
        plugin_SS.standbyLimit = 0;
        ewf.SpecialEventHandler.SpecialEventFlag = true;
        ewf.help.help_menu_index = 0;
    },

    removeAll: function () {
        // mingxiang 20090612
        // internet
        if (ewf.InternetModule.BrowserWin != null) {
            Nimbus.setAutoSpacialNavigationEnable(false);
            ewf.InternetModule.BrowserWin.destroy();
            ewf.InternetModule.BrowserWin.removeFocus();
            Nimbus.setMousePointerEnable(false);
        }
        if (ewf.InternetModule.internetTimeout != null) {
            clearTimeout(ewf.InternetModule.internetTimeout);
            clearInterval(ewf.InternetModule.internetURLTimer);
            ewf.InternetModule.internetTimeout = null;
            ewf.InternetModule.internetURLTimer = null;
        }
        // mingxiang end

        if ($("StreamPanel") != null) {
            if (typeof window['EONimbus'] != 'undefined') {
                var player = Nimbus.getPlayer();
                var position = player.getPosition();
                if (player.isError() == 0) {
                    var url = '../stbservlet?attribute=ote_update_ticket_suspend_position&home_id='
                        + ewf.DataCenter.Customer.home_id + '&ticket_id=' + ewf.RTSPStream.ticket_id + '&suspend_position=' + position;
                    var obj = new Ajax(url, null, null, 0);
                    if (obj != null) {
                        obj.get();
                    }
                }
                player.close();
            }
            clearTimeout(ewf.RTSPStream.Description_timeout);
            clearTimeout(ewf.RTSPStream.timeout);
            if (ewf.RTSPStream.isPlayOK_timeout != null) {
                clearTimeout(ewf.RTSPStream.isPlayOK_timeout);
            }
            if (ewf.RTSPStream.GracePeriodTimer != null) {
                clearTimeout(ewf.RTSPStream.GracePeriodTimer);
            }
        }
        if (typeof window['EONimbus'] != 'undefined') {
            var player = Nimbus.getPlayer();
            if (player != null) {
                player.close();
            }
        }
        clearTimeout(ewf.Platform.clock_timeout);
        if (typeof window['EONimbus'] != 'undefined') {
            Nimbus.EventListeners[0] = null;
        }
        else {
            this.previousEvent = document.eventListeners[0];
            document.removeEventListener('keydown', this.previousEvent, false);
            document.eventListeners[0] = null;
        }
        document.onkeydown = null;
        if ($('ChannelPanel') != null) {
            document.removeEventListener('keydown', ewf.ChannelGroup.channel_listener, false);
        }
        document.body.innerHTML = '';
        document.body.style.background = "#000000 url() no-repeat fixed";
        plugin_SS.streaming = false;
        plugin_SS.standbyLimit = 0;
        ewf.SpecialEventHandler.SpecialEventFlag = true;
        ewf.help.help_menu_index = 0;
    },

    goto_Internet: function () {
        ewf.SpecialEventHandler.removeAll_and_Init();
        ewf.Platform.major_down();
        ewf.Platform.major_eval();
    },

    goto_WynnServices: function () {
        ewf.SpecialEventHandler.removeAll_and_Init();
        ewf.Platform.major_down();
        ewf.Platform.major_down();
        ewf.Platform.major_eval();
    },

    goto_Movies: function () {
        var url = '../stbservlet?attribute=ewf_list_ticket'
            + '&home_id=' + ewf.DataCenter.Customer.home_id
            + '&product_type=0';
        var obj = new Ajax(url, this.goto_Movies_resp, null, 0);
        if (obj != null) obj.get();
    },

    goto_Movies_resp: function (response, param) {
        eval('var obj = ' + response + ';');
        if (obj.length == 0) {
            var url = '../stbservlet?attribute=ewf_list_service_package'
                + '&home_id=' + ewf.DataCenter.Customer.home_id;
            var obj = new Ajax(url, ewf.SpecialEventHandler.goto_Movies_resp_resp, null, 0);
            if (obj != null) obj.get();
        }
        else {
            ewf.SpecialEventHandler.removeAll();
            ewf.Platform.call('bookmarks', true);
        }
    },

    goto_Movies_resp_resp: function (response, param) {
        eval('var obj = ' + response + ';');
        if (obj.length == 0) {
            ewf.SpecialEventHandler.removeAll_and_Init();
            ewf.Platform.major_eval();
        }
        else {
            ewf.SpecialEventHandler.removeAll();
            ewf.Platform.call('bookmarks', true);
        }
    },

    goto_Languages: function () {
        ewf.SpecialEventHandler.removeAll_and_Init();
        ewf.Platform.major_down();
        ewf.Platform.major_down();
        ewf.Platform.major_down();
        ewf.Platform.major_down();
        ewf.Platform.major_down();
        ewf.Platform.major_eval();
    },

    goto_MusicMoods: function () {
        ewf.SpecialEventHandler.removeAll();
        ewf.Platform.go_to('music_moods', true);

    },

    goto_Internet: function () {
        ewf.SpecialEventHandler.removeAll_and_Init();
        ewf.Platform.major_down();
        ewf.Platform.major_eval();
    },

    goto_ProgramGuide: function () {
        ewf.SpecialEventHandler.removeAll();
        ewf.Platform.call('program_guide');
    },

    goto_LocalTelevision: function (channelId) {
        ewf.SpecialEventHandler.removeAll();
        if (channelId != null) var channel_id = channelId;
        else var channel_id = ewf.ChannelGroup.channel_id;
        if (typeof channel_id == 'undefined') channel_id = 0;
        ewf.Platform.tune('television_channels', ewf.conf.FTGChannelLineUpID, channel_id, true);
    },

    goto_HotelBill: function () {
        ewf.SpecialEventHandler.removeAll();
        ewf.Platform.call('hotel_bill', true);
    },

    goto_Help: function () {
        ewf.SpecialEventHandler.removeAll();
        ewf.help.go_to('help', true);
    },

    Sleep: function () {
        var tv = Nimbus.getTVController();
        if (tv.getPower() == true) {
            tv.setPower(false);
        }
        else {
            tv.setPower(true);
        }
    },

    PowerOn: function () {
        var tv = Nimbus.getTVController();
        Nimbus.printOut('[SeaChange Application] ====== Now Power Status: ' + tv.getPower());
        if (tv.getPower() == true) {
            if (ewf.DataCenter.Customer.checkout == false) {
                ewf.SpecialEventHandler.removeAll_and_Init();
            } else if (plugin_SS.streaming != false) {
                ewf.ChannelGroup.quit();
            } else {
                if (typeof window['EONimbus'] != 'undefined') {
                    //Nimbus.EventListeners[0]=ewf.SpecialEventHandler.NullEventNow;
                }
            }
            if (ewf.SpecialEventHandler.reloadPage_Timeout != null) {
                clearTimeout(ewf.SpecialEventHandler.reloadPage_Timeout);
            }
            ewf.SpecialEventHandler.reloadPage_Timeout = setTimeout('Nimbus.reloadPage();', 2 * 60 * 60 * 1000);
            Nimbus.printOut('[SeaChange Application] ====== Set Power Off');
            tv.setPower(false);
            Nimbus.printOut('[SeaChange Application] ====== Now Power Status: ' + tv.getPower());
        }
        else {
            if (ewf.DataCenter.Customer.checkout == false) {
                ewf.SpecialEventHandler.removeAll_and_Init();
            } else if (plugin_SS.streaming != false) {
                ewf.ChannelGroup.quit();
            } else {
                if (typeof window['EONimbus'] != 'undefined') {
                    Nimbus.EventListeners[0] = ewf.SpecialEventHandler.NullEventNow;
                }
            }
            if (Ajax.timer != null) {
                clearTimeout(Ajax.timer);
                Ajax.timer = null;
            }
            if (ewf.SpecialEventHandler.reloadPage_Timeout != null) clearTimeout(ewf.SpecialEventHandler.reloadPage_Timeout);
            if (Ajax.timer != null) {
                clearTimeout(Ajax.timer);
                Ajax.timer = null;
            } // kill one more time
            ewf.ChannelGroup.getUserData(true);
            Nimbus.printOut('[SeaChange Application] ====== Set Power On');
            tv.setPower(true);
            Nimbus.printOut('[SeaChange Application] ====== Now Power Status: ' + tv.getPower());
            tv.setVolume(10);
        }
    },

    PowerQuery: function () {
        var tv = Nimbus.getTVController();
        var powerState = tv.getPower();
        if (ewf.SpecialEventHandler.lastPowerState == null) {	// might be first time, assign the state
            ewf.SpecialEventHandler.lastPowerState = powerState;
            return;
        }

        if (powerState == ewf.SpecialEventHandler.lastPowerState)	// state not changed
            return;

        ewf.SpecialEventHandler.lastPowerState = powerState;	// assign new state

        if (powerState == false)	// power from ON to OFF
        {
            Nimbus.printOut('SEAC: power state detected from ON to OFF!');
            if (ewf.DataCenter.Customer.checkout == false) {
                ewf.SpecialEventHandler.removeAll_and_Init();
            } else if (plugin_SS.streaming != false) {
                ewf.ChannelGroup.quit();
            } else {
                if (typeof window['EONimbus'] != 'undefined') {
                    Nimbus.EventListeners[0] = null;
                }
            }
            if (ewf.SpecialEventHandler.reloadPage_Timeout != null) {
                clearTimeout(ewf.SpecialEventHandler.reloadPage_Timeout);
            }
            ewf.SpecialEventHandler.reloadPage_Timeout = setTimeout('Nimbus.reloadPage();', 2 * 60 * 60 * 1000);
        }
        else	// power from OFF to ON
        {
            Nimbus.printOut('SEAC: power state detected from OFF to ON!');
            tv.setVolume(25);
            if (ewf.DataCenter.Customer.checkout == false) {
                ewf.SpecialEventHandler.removeAll_and_Init();
            } else if (plugin_SS.streaming != false) {
                ewf.ChannelGroup.quit();
            } else {
                if (typeof window['EONimbus'] != 'undefined') {
                    Nimbus.EventListeners[0] = ewf.SpecialEventHandler.NullEventNow;
                }
            }
            ewf.ChannelGroup.getUserData();
            if (Ajax.timer != null) {
                clearTimeout(Ajax.timer);
                Ajax.timer = null;
            }
            if (ewf.SpecialEventHandler.reloadPage_Timeout != null) {
                clearTimeout(ewf.SpecialEventHandler.reloadPage_Timeout);
            }

            if (Ajax.timer != null) {
                clearTimeout(Ajax.timer);
                Ajax.timer = null;
            } // kill one more time
        }
    },

    Message: function () {

    },

    CloseCaption: function () {

    },

    TV_Internet: function () {

    },

    show: function (str) {
        if (this.timeout != null) {
            clearTimeout(this.timeout);
        }
        if (ewf.SpecialEventHandler.select_count == 0) {
            ewf.SpecialEventHandler.ComplexKey = str;
            ewf.SpecialEventHandler.select_count++;
            this.timeout = setTimeout(this.hide, 2000);
        }
        else if (ewf.SpecialEventHandler.select_count == 1) {
            ewf.SpecialEventHandler.ComplexKey = ewf.SpecialEventHandler.ComplexKey * 10 + str;
            ewf.SpecialEventHandler.select_count++;
            this.timeout = setTimeout(this.hide, 2000);
        }
        else if (ewf.SpecialEventHandler.select_count == 2) {
            ewf.SpecialEventHandler.ComplexKey = ewf.SpecialEventHandler.ComplexKey * 10 + str;
            ewf.SpecialEventHandler.select_count++;
            this.timeout = setTimeout(this.hide, 2000);
        }
        else if (ewf.SpecialEventHandler.select_count == 3) {
            ewf.SpecialEventHandler.ComplexKey = ewf.SpecialEventHandler.ComplexKey * 10 + str;
            ewf.SpecialEventHandler.select_count++;
            this.timeout = setTimeout(this.hide, 2000);
        }
        else if (ewf.SpecialEventHandler.select_count == 4) {
            ewf.SpecialEventHandler.ComplexKey = ewf.SpecialEventHandler.ComplexKey * 10 + str;
            ewf.SpecialEventHandler.select_count++;
            this.timeout = setTimeout(this.hide, 2000);
        }
        else if (ewf.SpecialEventHandler.select_count == 5) {
            ewf.SpecialEventHandler.ComplexKey = ewf.SpecialEventHandler.ComplexKey * 10 + str;
            this.hide();
        }
    },

    hide: function () {
        if (this.timeout != null) {
            clearTimeout(this.timeout);
        }
        if (ewf.SpecialEventHandler.select_count == 5 && ewf.SpecialEventHandler.ComplexKey == ewf.conf.STBProvisionPassword) {
            ewf.SpecialEventHandler.ComplexKey = 0;
            ewf.SpecialEventHandler.select_count = 0;
            ewf.SpecialEventHandler.removeAll();
            ewf.ProvisionModule.init();
        }
        else {
            ewf.SpecialEventHandler.select_count = 0;
            ewf.SpecialEventHandler.ComplexKey = 0;
        }
    },

    goto_AVInput: function () {
        ewf.SpecialEventHandler.removeAll_and_Init();
        ewf.Platform.switch_to("3", true);
    },

    goto_VGAInput: function () {
        ewf.SpecialEventHandler.removeAll_and_Init();
        ewf.Platform.switch_to("7", true);
    },

    goto_HDMIInput: function () {
        ewf.SpecialEventHandler.removeAll_and_Init();
        ewf.Platform.switch_to("4", true);
    },

    NullEventNow: function (event) {
        Nimbus.printOut("There is no Event now!");
        return true;
    }
};


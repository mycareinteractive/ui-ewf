﻿//  version 1.0.0.3.091026 for portal basic


/**
 * @library
 **/

window['ewf'] = {};
ewf.Customer = {};


ewf.onload = function () {

    // if something happens reload in 30 min
    window.setTimeout(function () {
        location.href = '/ewf/';
    }, 30 * 60 * 1000);

    window.detectPlatform(function () {
        var config = new Ajax('config.json', ewf.init, null, null, ewf.noConfig);
        config.get();
    });
};

ewf.init = function (conf) {
    ewf.conf = JSON.parse(conf) || {};
    document.eventListeners = new Array();
    ewf.Customer.init();
    document.addEventListener('keydown', ewf.SpecialEventHandler.SpecialEventListener, false);
    if (window.platform == 'ENSEO' && ewf.conf.updateContent) {
        Nimbus.updateContent(ewf.conf.updateContentLocation,
            ewf.conf.updateContentPort,
            ewf.conf.updateContentProtocol,
            ewf.conf.updateContentUsername,
            ewf.conf.updateContentPassword,
            'update.xml',
            ewf.conf.updateContentFolder,
            'System Drive');
    }
};

ewf.noConfig = function (status, statusText) {
    var alertText = 'Unable to load config.json - ' + status + ' ' + statusText;
    ewf.dialogView.DialogPanelBegin(alertText, 0, 60000, null);
};

/**
 * @global
 **/

window.detectPlatform = function (callback) {
    window.platform = 'PC';
    var async = false;
    var args = Array.prototype.slice.call(arguments, 1);

    if (typeof window['EONimbus'] != 'undefined') {
        window.platform = 'ENSEO';
    }
    else if (typeof window['EONebula'] != 'undefined') {
        window.platform = 'NEBULA';
    }
    else if (navigator.userAgent.indexOf(';LGE ;') > -1) {
        window.platform = 'LG';
        window.LG = {};
        async = true;
        hcap.network.getNetworkDevice({
            index: 0,
            onSuccess: function (s) {
                window.LG.deviceId = (s.mac || '').replace(/:/gi, '').toUpperCase();
                if (callback)
                    callback.apply(ewf, args);
            },
            onFailure: function (res) {
                window.LG.deviceId = 'ERROR';
                console.log('hcap.network.getNetworkDevice failed!');
                if (callback)
                    callback.apply(ewf, args);
            }
        });
    }

    if (!async) {
        if (callback)
            callback.apply(ewf, args);
    }
};

window.debug = function (msg) {
    if (console && console.log) {
        console.log(msg);
    }

    if (window.platform == 'ENSEO' && window.Nimbus) {
        Nimbus.printOut(msg);
    }
};

window['$'] = function (id) {
    return document.getElementById(id);
};

window.getScript = function (url, callback) {
    var head = document.getElementsByTagName("head")[0] || document.documentElement;
    var script = document.createElement("script");
    script.src = url;

    var done = false;

    script.onload = script.onreadystatechange = function () {
        if (!done && (!this.readyState ||
            this.readyState === "loaded" || this.readyState === "complete")) {
            done = true;
            if (callback)
                callback.apply(script, Array.prototype.slice.call(arguments, 2));
        }
    };

    head.insertBefore(script, head.firstChild);
};

if (typeof window['Ajax'] == 'undefined') {
    window['Ajax'] = function (url, callback, param, type, error) {
        this.url = url;
        if (Ajax.timer != null) {
            clearTimeout(Ajax.timer);
            Ajax.timer = null;
        }
        Ajax.timer = null;
        var request = this.request = window.XMLHttpRequest ? new XMLHttpRequest() : null;
        request.onreadystatechange = function () {
            Ajax.prototype.handleStateChange(request, callback, param, type, error);
        }

        debug('--- get HTTPRequest ---' + this.url);

        Ajax.timer = setTimeout(Ajax.prototype.timeout, 20 * 1000);
    };
    Ajax.timer;
    Ajax.prototype.handleStateChange = function (request, callback, param, type, error) {
        if (request.readyState == 4 && (request.status == 200 || request.status == 0)) {
            var response = type ? request.responseXML : request.responseText;

            debug('--- HTTPRequest response ---');
            debug(JSON.stringify(response));

            if (Ajax.timer != null) {
                clearTimeout(Ajax.timer);
                Ajax.timer = null;
            }
            if (typeof callback == 'function' && callback != null) {
                callback(response, param);
            }
        }
        else if (request.readyState == 4) {
            debug('--- HTTPRequest ERROR ---');
            if (Ajax.timer != null) {
                clearTimeout(Ajax.timer);
                Ajax.timer = null;
            }
            if (typeof error == 'function' && error != null) {
                error(request.status, request.statusText);
            }
        }
    };
    Ajax.prototype.get = function () {
        this.request.open('GET', this.url, true);
        this.request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        this.request.send(null);
    };
    // test wating GUI by Bernie
    Ajax.prototype.timeout = function () {
        document.body.innerHTML = '';
        document.body.style.backgroundImage = '';
        document.body.style.color = '#ffffff';
        document.body.style.backgroundColor = '#000000';
        document.body.innerHTML = '<br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sorry we are having some network issues.  Please wait...';
        if (Ajax.timer != null) {
            clearTimeout(Ajax.timer);
            Ajax.timer = null;
        }
        if (window.platform == 'ENSEO') {
            Nimbus.reload();
        }
        else if (window.platform == 'NEBULA') {
            Nebula.reload(false);
        }
        else {
            window.location.reload();
        }
    }
}

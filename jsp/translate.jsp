<%@ page import="com.biztunnel.lib.foundation.web.WebKeys,com.biztunnel.lib.foundation.deployment.ListOfXsl,com.biztunnel.lib.foundation.deployment.Xsl,com.biztunnel.lib.foundation.CommonDefine,com.biztunnel.lib.foundation.message.ObjectKeys,com.biztunnel.lib.util.tracer.Debug,java.io.StringReader,java.io.StringWriter,java.io.Reader,java.io.Writer,java.io.File,java.util.Vector,javax.xml.transform.Transformer,javax.xml.transform.TransformerFactory,javax.xml.transform.Templates,javax.xml.transform.stream.StreamSource,javax.xml.transform.stream.StreamResult,javax.xml.transform.dom.DOMSource" %>
<%
	try {
		String xmlString = null;
		Vector xmlVector = (Vector)request.getAttribute(WebKeys.ResultXMLKey);
		ListOfXsl listOfXsl = (ListOfXsl)request.getAttribute(WebKeys.XslListKey);
		Object[] objs = ((listOfXsl.getListOfXsl()).values()).toArray();
		Integer theNO = ((Xsl)objs[0]).getKey();
		String xsl_name = ((Xsl)objs[0]).getValue();
		if (theNO.intValue() == -1) {
			xmlString = "<ActionList>";
			for (int i=0; i<xmlVector.size(); i++)
			xmlString += "</ActionList>";
		} else if (theNO.intValue() == 0) xmlString = "<Root/>";
		else xmlString = (String)xmlVector.elementAt(theNO.intValue() - 1);
		String xsl = pageContext.getServletContext().getRealPath(xsl_name);
		StringWriter sw = new StringWriter();
		StringReader sr = new StringReader(xmlString);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer(new StreamSource(new File(xsl)));
		t.transform(new StreamSource((Reader)sr), new StreamResult(sw));
		String temp = sw.toString();
		int length = temp.getBytes("UTF-8").length;
		response.setIntHeader("content-Length", length+1);
		out.print(temp);
	} catch (Exception e) {
		Debug.print(e);
		String errorCode = CommonDefine.PROCESS_WEB + CommonDefine.EM_SYSTEM_ERROR + ObjectKeys.KEY_XMLTRANSALATEJSP + "01";
		request.setAttribute(WebKeys.ResultKey, errorCode);
		pageContext.getServletContext().getRequestDispatcher("/jsp/error.jsp").include(request, response);
	}
%>

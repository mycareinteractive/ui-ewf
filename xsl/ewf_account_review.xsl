<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    <xsl:template match="ControlArea" />
    <xsl:template match="DataArea/ListOfBill">
        [<xsl:apply-templates />]
    </xsl:template>
	<xsl:template match="Bill">
			<xsl:param name="str"><xsl:value-of select="@userName"/></xsl:param>
			<xsl:param name="str2"><xsl:value-of select="translate($str, '&quot;','&#233;')"/></xsl:param>
			<xsl:if test="position() != 2">, </xsl:if>{ AccountNumber: "<xsl:value-of select="@accountNumber" />", Balance: "<xsl:value-of select="@balance" />", UserName: "<xsl:value-of select='translate($str2,"&#233;","&apos;")' />", Bill: <xsl:apply-templates /> }
	</xsl:template>
	<xsl:template match="ListOfCharge">
	[
		<xsl:for-each select="Charge">
		{ Entry: {
			chargeNumber: "<xsl:value-of select="@chargeNumber" />"
			, title: "<xsl:value-of select="@title" />"
			, postingDate: "<xsl:value-of select="@postingDate" />"
			, amount: "<xsl:value-of select="@amount" />"
			, tax: "<xsl:value-of select="@tax" />"
			, total: "<xsl:value-of select="@total" />"
		} }<xsl:if test="position() != last()">, </xsl:if>
		</xsl:for-each>
	]
	</xsl:template>
    <xsl:template match="DataArea/ListOfEntry/PageInfo" />
    <xsl:template match="EntrySearchCondition" />
</xsl:stylesheet>

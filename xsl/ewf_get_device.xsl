<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    <xsl:template match="ControlArea" />
    <xsl:template match="DataArea/Device">
        {
            homeID: "<xsl:value-of select="@homeID" />"
            , siteID: "<xsl:value-of select="@siteID" />"
            , TVNumber: "<xsl:value-of select="@TVNumber" />"
            , TVInputMode: "<xsl:value-of select="@TVInputMode" />"
        }
    </xsl:template>
</xsl:stylesheet>

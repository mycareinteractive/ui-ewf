<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    <xsl:template match="ControlArea" />
    <xsl:template match="DataArea/STBUser/Home">
        {
		<xsl:param name="str"><xsl:for-each select="ListOfSTBUser/STBUser">|<xsl:value-of select="@userName"/></xsl:for-each></xsl:param>
		<xsl:param name="str2"><xsl:value-of select="translate($str, '&quot;','&#233;')"/></xsl:param>
            home_id: "<xsl:value-of select="@homeID" />"
            , site_id: "<xsl:value-of select="@siteID" />"
            , device_id: "<xsl:value-of select="ListOfDevice/Device/@deviceID" />"
            , device_name: "<xsl:value-of select='translate($str2,"&#233;","&apos;")' />"
            , rating: "<xsl:value-of select="@rating" />"
            , blockpin: "<xsl:value-of select="@pin" />"
            , language: "<xsl:value-of select="@language" />"
            ,TVNumber: "<xsl:value-of select="ListOfDevice/Device/@TVNumber" />"
            ,TVInputMode: "<xsl:value-of select="ListOfDevice/Device/@TVInputMode" />"
            , portalID: "<xsl:value-of select="@portalID" />"
			, disable_all_pay_service: "<xsl:if test="count(ListOfDevice/Device/ListOfServiceControl/ServiceControl) != 0">false</xsl:if>"
			, disable_internet: "<xsl:if test="count(ListOfDevice/Device/ListOfServiceControl/ServiceControl) != 0">false</xsl:if>"
        }
    </xsl:template>
</xsl:stylesheet>

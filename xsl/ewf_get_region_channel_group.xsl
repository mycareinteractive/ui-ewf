<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    <xsl:template match="ControlArea" />
    <xsl:template match="DataArea/RegionChannelGroup">
        {
            ListOfChannel: [
                <xsl:for-each select="ListOfChannel/Channel">
                    { Channel: {
                        channelID: "<xsl:value-of select="@channelID" />"
                        , channelName: "<xsl:value-of select="@channelName" />"
						, shortName: "<xsl:value-of select="ListOfMetaData/MetaData[@mdUID='1622']/." />"
                        , frequency: "<xsl:value-of select="@frequency" />"
                        , programNumber: "<xsl:value-of select="@programNumber" />"
                        , channelNumber: "<xsl:value-of select="@channelNumber" />"
                        , ipAddress: "<xsl:value-of select="@ipAddress" />"
                        , ipPort: "<xsl:value-of select="@port" />"
                        , QAMMode: "<xsl:value-of select="@QAMMode" />"
                    } }<xsl:if test="position() != last()">, </xsl:if>
                </xsl:for-each>
            ], 
            ListOfChannelLineUp: [
                <xsl:for-each select="ListOfChannelLineUp/ChannelLineUp">
                    { ChannelLineUp: {
                        channelLineUpID: "<xsl:value-of select="@channelLineUpID" />"
                        , channelLineUpName: "<xsl:value-of select="@channelLineUpName" />"
                        , channels: "<xsl:value-of select="@channels" />"
                        , regionGroupAppMode: "<xsl:value-of select="@regionGroupAppMode" />"
                    } }<xsl:if test="position() != last()">, </xsl:if>
                </xsl:for-each>
            ]
        }
    </xsl:template>
    <xsl:template match="DataArea/ListOfEntry/PageInfo" />
    <xsl:template match="EntrySearchCondition" />
</xsl:stylesheet>

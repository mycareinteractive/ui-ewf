<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:template match="/"><xsl:apply-templates /></xsl:template>
	<xsl:template match="ControlArea" />
	<xsl:template match="DataArea"><xsl:apply-templates /></xsl:template>
	<xsl:template match="ProductOffering">{ ProductOffering: {
		productOfferingID: "<xsl:value-of select="@productOfferingID" />", 
		productType: "<xsl:value-of select="@productType" />", 
		suspendlistLiveTime: "<xsl:value-of select="@fixedRentalDuration" />", 
		price: "<xsl:value-of select="@price" />"
	} }</xsl:template>
    <xsl:template match="DataArea/ListOfEntry/PageInfo" />
    <xsl:template match="EntrySearchCondition" />
</xsl:stylesheet>
<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    <xsl:template match="ControlArea" />
    <xsl:template match="DataArea/ListOfTicket">[ <xsl:apply-templates /> ]</xsl:template>
	<xsl:template match="Ticket">
		<xsl:param name="str"><xsl:value-of select="Asset/ListOfMetaData/MetaData[@mdUID='1']"/></xsl:param>
		<xsl:param name="str2"><xsl:value-of select="translate($str, '&quot;','&#233;')"/></xsl:param>
        <xsl:if test="position() != 2">, </xsl:if>{ Entry: {
            ticketID: "<xsl:value-of select="@ticketID" />"
            , entryUID: "<xsl:value-of select="@assetID" />"
            , localEntryUID: "<xsl:value-of select="@localEntryUID" />"
            , assetName: "<xsl:value-of select='translate($str2,"&#233;","&apos;")' />"
            , rating: "<xsl:value-of select="Asset/ListOfMetaData/MetaData[@mdUID='21']" />"
            , expirationTime: "<xsl:value-of select="@expirationTime" />"
            , ticketIDList: "<xsl:value-of select="@ticketIDList" />"
            , suspendPosition: "<xsl:value-of select="@suspendPosition" />"
            , isCharge:"<xsl:value-of select="@isCharge"/>"
        } }
	</xsl:template>
    <xsl:template match="DataArea/ListOfTicket/PageInfo" />
    <xsl:template match="SearchConditionType" />
</xsl:stylesheet>

<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:template match="/">{ IPAddress: "<xsl:value-of select="//DataArea/ServerList/CMGroup/Instance/IPAddress/." />", Port: "<xsl:value-of select="//DataArea/ServerList/CMGroup/Instance/Port/." />" }</xsl:template>
</xsl:stylesheet>
<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    <xsl:template match="ControlArea" />
    <xsl:template match="DataArea/ListOfBill"> [ <xsl:apply-templates /> ] </xsl:template>
    <xsl:template match="Bill">
				<xsl:param name="str"><xsl:value-of select="@userName"/></xsl:param>
				<xsl:param name="str2"><xsl:value-of select="translate($str, '&quot;','&#233;')"/></xsl:param>
        { Entry: {
        	userName: "<xsl:value-of select='translate($str2,"&#233;","&apos;")' />"
            , accountNumber: "<xsl:value-of select="@accountNumber" />"
            , balance: "<xsl:value-of select="@balance" />"
        } }<xsl:if test="position() != last()">, </xsl:if>
    </xsl:template>
    <xsl:template match="DataArea/ListOfEntry/PageInfo" />
    <xsl:template match="EntrySearchCondition" />
</xsl:stylesheet>

<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    <xsl:template match="ControlArea" />
    <xsl:template match="DataArea/Asset">
	<xsl:param name="str"><xsl:value-of select="ListOfMetaData/MetaData[@mdUID='1']"/></xsl:param>
	<xsl:param name="str2"><xsl:value-of select="translate($str, '&quot;','&#233;')"/></xsl:param>
        {
            localEntryUID: "<xsl:value-of select="@localEntryUID" />"
            , entryName: "<xsl:value-of select='translate($str2,"&#233;","&apos;")' />"
            , duration: "<xsl:value-of select="@duration" />"
            , entryUID: "<xsl:value-of select="@entryUID" />"
            , price: "<xsl:value-of select="@price" />"
            , isOwned: "<xsl:value-of select="@isOwned" />"
            , ticketIDList: "<xsl:value-of select="@ticketIDList" />"
			, previewEntryUID: "<xsl:value-of select="ListOfEntry/AssociatedAsset/@entryUID" />"
            , ListOfMetaData: [<xsl:apply-templates />]
        }
    </xsl:template>
	<xsl:template match="MetaData" />
	<xsl:template match="//DataArea/Asset/ListOfMetaData/MetaData">
	<xsl:param name="md"><xsl:value-of select="."/></xsl:param>
	<xsl:param name="md2"><xsl:value-of select="translate($md, '&quot;','&#233;')"/></xsl:param>
		<xsl:if test="position() != 2">, </xsl:if>"@<xsl:value-of select="@mdUID" />|<xsl:value-of select='translate($md2,"&#233;","&apos;")' />"
	</xsl:template>
    <xsl:template match="EntrySearchCondition" />
</xsl:stylesheet>

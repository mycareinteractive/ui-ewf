<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    <xsl:template match="ControlArea" />
    <xsl:template match="DataArea/ListOfEntry">
        [
            <xsl:for-each select="ProductOffering">
            { Entry: {
                hierarchyUID: "<xsl:value-of select="@activeViewHUID" />"
                , entryName: "<xsl:value-of select="@entryName" />"
				, productOfferingID: "<xsl:value-of select="@productOfferingID" />"
            } }<xsl:if test="position() != last()">, </xsl:if>
            </xsl:for-each>
            , <xsl:for-each select="Folder">
            { Entry: {
				hierarchyUID: "<xsl:value-of select="@hierarchyUID" />"
				, entryName: "<xsl:value-of select="@entryName" />"
				, productOfferingID: "<xsl:value-of select="@productOfferingID" />"
                , description: "<xsl:value-of select="@description" />"
            } }<xsl:if test="position() != last()">, </xsl:if>
            </xsl:for-each>
        ]
    </xsl:template>
    <xsl:template match="DataArea/ListOfEntry/PageInfo" />
    <xsl:template match="EntrySearchCondition" />
</xsl:stylesheet>

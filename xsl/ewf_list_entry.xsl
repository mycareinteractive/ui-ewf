<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    <xsl:template match="ControlArea" />
    <xsl:template match="DataArea/ListOfEntry">
        [
			<xsl:apply-templates />
        ]
    </xsl:template>
	<xsl:template match="ProductOffering">
	<xsl:if test="position() != 2()">, </xsl:if>
	{ "Entry": {
		"hierarchyUID": "<xsl:value-of select="@activeViewHUID" />"
        , "localEntryUID": "<xsl:value-of select="@localEntryUID" />"
        , "entryName": "<xsl:value-of select="@entryName" />"
        , "entryType": "<xsl:value-of select="@entryType" />"
        , "productOfferingUID": "<xsl:value-of select="@productOfferingUID" />"
		, "rating": "<xsl:value-of select="@rating" />"
	} }
	</xsl:template>
	<xsl:template match="SymbolLink">
	<xsl:if test="position() != 2()">, </xsl:if>
	{ "Entry": {
		"hierarchyUID": "<xsl:value-of select="@hierarchyUID" />"
        , "localEntryUID": "<xsl:value-of select="@localEntryUID" />"
        , "entryName": "<xsl:value-of select="@entryName" />"
        , "entryType": "<xsl:value-of select="@entryType" />"
        , "productOfferingUID": "<xsl:value-of select="@productOfferingUID" />"
        , "targetHUID": "<xsl:value-of select="@targetHUID" />"
		, "rating": "<xsl:value-of select="@rating" />"
	} }
    </xsl:template>
	<xsl:template match="Folder|Space|View">
	<xsl:if test="position() != 2()">, </xsl:if>
	{ "Entry": {
		"hierarchyUID": "<xsl:value-of select="@hierarchyUID" />"
        , "localEntryUID": "<xsl:value-of select="@localEntryUID" />"
        , "entryName": "<xsl:value-of select="@entryName" />"
        , "entryType": "<xsl:value-of select="@entryType" />"
        , "productOfferingUID": "<xsl:value-of select="@productOfferingUID" />"
		, "rating": "<xsl:value-of select="@rating" />"
	} }
    </xsl:template>
	<xsl:template match="Asset">
	<xsl:param name="str"><xsl:value-of select="ListOfMetaData/MetaData[@mdUID='1']"/></xsl:param>
	<xsl:param name="str2"><xsl:value-of select="translate($str, '&quot;','&#233;')"/></xsl:param>
	<xsl:if test="position() != 2()">, </xsl:if>
	{ "Entry": {
	"hierarchyUID": "<xsl:value-of select="@hierarchyUID" />"
	, "localEntryUID": "<xsl:value-of select="@localEntryUID" />"
	, "entryName": "<xsl:value-of select='translate($str2,"&#233;","&apos;")' />"
    , "entryType": "<xsl:value-of select="@entryType" />"
    , "productOfferingUID": "<xsl:value-of select="@productOfferingUID" />"
	, "rating": "<xsl:value-of select="ListOfMetaData/MetaData[@mdUID='21']" />"
	, "poster": [""<xsl:apply-templates />]
    } }
    </xsl:template>
    <xsl:template match="DataArea/ListOfEntry/PageInfo" />
    <xsl:template match="EntrySearchCondition" />
    <xsl:template match="MetaData" />
		<xsl:template match="DataArea/ListOfEntry/Asset/ListOfMetaData/MetaData">
				
						<xsl:param name="md"><xsl:value-of select="."/></xsl:param>
						<xsl:param name="md2"><xsl:value-of select="translate($md, '&quot;','&#233;')"/></xsl:param>
						<xsl:if test="@mdUID='39'">
								<xsl:if test="position() !=0()">,</xsl:if>"<xsl:value-of select='translate($md2,"&#233;","&apos;")' />"
						</xsl:if>
	</xsl:template>
</xsl:stylesheet>

<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:template match="/"><xsl:apply-templates /></xsl:template>
    <xsl:template match="ControlArea" />
    <xsl:template match="DataArea"><xsl:apply-templates /></xsl:template>
    <xsl:template match="SurfPlaylist">{ SurfPlaylistID: "<xsl:value-of select="@id" />", ItemList:[<xsl:apply-templates />] }</xsl:template>
    <xsl:template match="PlaylistItem"><xsl:if test="position() != 2">, </xsl:if>"<xsl:value-of select="@startNPT" />"</xsl:template>
</xsl:stylesheet>

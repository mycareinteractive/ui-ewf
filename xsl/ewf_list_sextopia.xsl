<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    <xsl:template match="ControlArea" />
    <xsl:template match="DataArea/ListOfEntry">
        [
			<xsl:for-each select="ProductOffering">
				{ ProductOffering: {
					entryName: "<xsl:value-of select="@entryName" />"
					, productOfferingType: "<xsl:value-of select="@productOfferingType" />"
					, productOfferingUID: "<xsl:value-of select="@productOfferingUID" />"
					, productType: "<xsl:value-of select="@productType" />"
					<xsl:if test="count(View/Asset) != 0">
					, ListOfAsset: [<xsl:apply-templates />]
					</xsl:if>
					<xsl:if test="count(View/Folder) != 0">
					, ListOfFolder: [
						<xsl:for-each select="View/Folder">
							{ Folder: {
								entryName: "<xsl:value-of select="@entryName" />"
								, description: "<xsl:value-of select="@description" />"
								, ListOfAsset: [<xsl:apply-templates />]
							} }<xsl:if test="position() != last()">, </xsl:if>
						</xsl:for-each>
					]
					</xsl:if>
				} }<xsl:if test="position() != last()">, </xsl:if>
			</xsl:for-each>
        ]
    </xsl:template>
	<xsl:template match="Asset">
	<xsl:param name="str"><xsl:value-of select="ListOfMetaData/MetaData[@mdUID='1']"/></xsl:param>
	<xsl:param name="str2"><xsl:value-of select="translate($str, '&quot;','&#233;')"/></xsl:param>
	{ Asset: {
		entryName: "<xsl:value-of select='translate($str2,"&#233;","&apos;")' />"
		, localEntryUID: "<xsl:value-of select="@localEntryUID" />"
		, productOfferingUID: "<xsl:value-of select="@productOfferingUID" />"
		, entryUID: "<xsl:value-of select="@entryUID" />"
		, rating: "<xsl:value-of select="ListOfMetaData/MetaData[@mdUID='21']" />"
	} }<xsl:if test="position() != last()">, </xsl:if>
	</xsl:template>
	<xsl:template match="Space|Space/Asset" />
	<xsl:template match="description" />
    <xsl:template match="DataArea/ListOfEntry/PageInfo" />
    <xsl:template match="EntrySearchCondition" />
</xsl:stylesheet>

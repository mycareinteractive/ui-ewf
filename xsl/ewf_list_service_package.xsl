<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no" />
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    <xsl:template match="ControlArea" />
    <xsl:template match="DataArea/ListOfPackage">
        [
            <xsl:for-each select="Package">
            { Entry: {
            	productOfferingName: "<xsl:value-of select="@productOfferingName" />"
                , expirationTime: "<xsl:value-of select="@expirationTime" />"
                , productOfferingID: "<xsl:value-of select="@productOfferingID" />"
            } }<xsl:if test="position() != last()">, </xsl:if>
            </xsl:for-each>
        ]
    </xsl:template>
    <xsl:template match="DataArea/ListOfEntry/PageInfo" />
    <xsl:template match="EntrySearchCondition" />
</xsl:stylesheet>
